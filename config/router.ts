

export const routes = [
    {
        path:"/login",
        component:"@/pages/login/index",
        menuRender:false, // 是否渲染menu
        headerRender:false // 是否渲染header
    },
    {
        path:"/registry",
        menuRender:false,
        target:"_blank", // 新的页面打开
        headerRender:false,
        component:"@/pages/registry/index",
    },
    {
        path:"/index",
        component:"@/pages/index/index",
        name:"工作台",
    },
    {
        path:"/user",
        component:"@/pages/user/index",
        name:"用户管理",
        access:"isAdmin", // 在渲染组件前判断 access.isAdmin 是否为true  为true 的时候 正常渲染页面 为false 的时候 返回403
    }, 
    {
        path:"/",
        redirect:"/login"
    }
];