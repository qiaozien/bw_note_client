import { defineConfig } from "umi";
import { routes } from "./router";

//  用来定义配置的
//  .umirc.ts它的优先级 要比config要高
export default defineConfig({
    title:"创作平台后台管理",
    routes,
    layout:{
        name:"八维创作平台",
        logo:"https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-11-12/%E5%85%AC%E5%8F%B8logo.png"
    },
    proxy:{
        "/api":{
            target:"https://creationapi.shbwyz.com",
            changeOrigin:true,
            pathRewrite:{
                "^/api":""
            }
        }
    },
    dva: {
        immer: true,
        hmr: false,
    },
});
