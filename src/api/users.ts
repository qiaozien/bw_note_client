import { request } from "umi";

import { BasePageParams } from "./api.d";


export const _getUserList = (params:BasePageParams= {page: 1, pageSize:10}) => request(`/api/api/user`,{
    params
});