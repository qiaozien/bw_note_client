import { request } from "umi";
import { BasePageParams } from "./api.d"

export const _get_artical = () => request(`/api/api/article`,{
    method:"get",
});


export const getChartData = () => new Promise((resolve) => {
    setTimeout(() => {
        resolve({
            success:true,
            msg:"获取数据成功",
            data:[
                new Array(7).fill("").map(() => Math.floor(Math.random() * 100)+ 1),
                new Array(7).fill("").map(() => Math.floor(Math.random() * 100)+ 1),
            ]
        })
    },500)
});

//  获取文章列表

export const _getArticalList = (params:BasePageParams = {page:1,pageSize:6}) => request(`/api/api/article`,{params});