export interface BasePageParams {
    pageSize:number,
    page:number
}