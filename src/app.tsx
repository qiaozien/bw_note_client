/**
 * request 统一配置 
 * 统一处理请求公共参数 ， 携带token 身份标识  csrf-token
 * 错误的统一处理
 * 
 * 
 **/
import { RequestConfig , history } from "umi";
import { Button , Dropdown , Menu} from 'antd';

interface Headers {
    authorization?:string | undefined | null,
    [propName:string]:any
}

//  启用initial-state 插件
export const getInitialState = () => { // 该函数 在页面初始化时 执行一次 而且只执行一次
    //  容错处理
    try {
        const userInfo = JSON.parse(localStorage.getItem("userInfo") as string); // 获取本地存储的用户信息
        if(!userInfo){
            history.replace("/login")
        }
        return userInfo;
    }catch(error) {
        console.log("error")
    }
};

//  layout

export const layout = () => {
    const menu = (
        <Menu
            onClick={({key , domEvent}) => {
                //  获取原生事件
                domEvent.stopPropagation();
                history.push(key);
            }}
            items={
                [
                    {
                        key:'/create/article',
                        label:'新建文章'
                    },
                    {
                        key:'/create/page',
                        label:'新建页面'
                    }
                ]
        }>

        </Menu>
    );

    return {
        rightContentRender:() => <div>123</div>,
        footerRender: () => <div>footer</div>,
        menuHeaderRender : (logo:any , title:string) => {
            return <div>
                {logo}
                {title}
                <Dropdown overlay={menu} placement="bottom">
                    <Button>新建文章</Button>
                </Dropdown>
            </div>
        }
    }
};

const TOKEN_TYPE = "Bearer";
export const request:RequestConfig = {
    timeout:20000,
    errorConfig:{
        adaptor:(resData) => {
            console.log(`修改相应结果adaptor`,resData);
            return {
                ...resData,
                errorMessage:resData.msg // success 为false的时候  弹框提示errorMessage信息
            }
        }
    },
    requestInterceptors:[ // 请求拦截器
        (url , options) => {
            console.log(`接口请求前执行，处理请求接口时携带的公共参数`,options);
            const headers:Headers = {
                ...options.headers
            };
            //  判断权限有没有
            if(!options.isAuthorization){ // 获取本地存储里面的token
                headers["authorization"] = `${TOKEN_TYPE} ${JSON.parse(localStorage.getItem("userInfo") as string).token}`;
            }
            return {
                url:`${url}`, // 请求地址
                options:{
                    ...options,
                    interceptors:true,
                    headers
                }
            }
        }
    ],
    responseInterceptors:[
        async response => {
            const res = await response.json();
            if(res.statusCode === 403){
                res.msg = `您暂时没有该接口权限`
            }
            if(res.statusCode === 401){
                history.replace("/login")
            }
            return res;
        }
    ]
}