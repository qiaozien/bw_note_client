import { FC , CSSProperties} from "react";

import BaseChart from "@/components/baseChart";
import { useRequest } from "ahooks";
import { getChartData , _getArticalList } from "@/api";
import { Card } from 'antd';
const gridStyle:CSSProperties = {
    width:"33%",
    textAlign:"center"
}




const Index:FC = () => {
    const { loading , data} = useRequest(getChartData , {
        pollingInterval:10000
    });
    const {loading:articleLoading , data: articleData} = useRequest(_getArticalList);
    
    
    return (
        <div>
            {/*  
                type 用来指定echarts图表的类型
            */}
            <BaseChart
                type="line"
                charData={{
                    title:{
                        text:"销售统计图",
                        subtext:"周统计图"
                    },
                    legend:{
                        type:"plain"
                    },
                }}
                series={
                    [
                        {
                            type:"line",
                            name:"访问量",
                            data:data?.data[0]
                        },
                        {
                            type:"bar",
                            name:"成交量",
                            data:data?.data[1]
                        }
                    ]
                }
                xData={["周一","周二","周三","周四","周五"]}
                loading={loading}
                width={500}
                height={400}
            />

            <Card
                title="最新文章"
                loading={articleLoading}
            >
                {
                    articleData?.data[0].map((item:any) => {
                        return <Card.Grid key={item.id} style={gridStyle}>
                            {item.title}
                        </Card.Grid>
                    })
                }
            </Card>
        </div>
    )
};


export default Index;