import {FC , useEffect } from "react";
import style from "./login.less";
import { useSelector , useDispatch } from "dva";
import { change_name } from "@/actions";
//  css module / css in js  相当于只在当前页面起作用 相当于vue中scoped属性
import { loginState } from "./models/index";
import { LoginFormPage , ProFormText } from "@ant-design/pro-components";
import { LoginData , _login} from "@/api/login";
import { useLocalStorageState } from "ahooks";
import { useModel , history} from "umi";
const Login:FC = () => {
    const dispatch = useDispatch();
    const [,setUserInfo] = useLocalStorageState("userInfo");
    const { refresh } = useModel("@@initialState");
    const { name , list} = useSelector(({loginModel}:{loginModel:loginState}) => {
        return {
            ...loginModel
        };
    });
    useEffect(() => {
        dispatch({
            type:"loginModel/getArtical"
        })
    },[dispatch]);
    const handleClickLogin = async (values:LoginData) => {
        const { data } = await _login(values);
        //  存储数据信息
        setUserInfo({
            ...data
        });
        
        // 让app下的getInitialState 重新执行 
        refresh();
        history.push("/index");
        
    }
    
    return (
        <div className={style.loginWrap}>
            <LoginFormPage 
                backgroundImageUrl="https://www.bwie.com/static/home/images/welcome-hero/banner21.jpg"
                logo="https://www.bwie.com/static/home/logo/logo.jpg"
                title="八维大舞台"
                subTitle="CMS管理平台"
                onFinish={handleClickLogin}
            >
                <>
                    <ProFormText 
                        name="name" 
                        labelAlign="left"
                        placeholder="请输入用户名"
                        label="账号"
                        rules={[
                            {
                                required:true,
                                message:"请输入用户名"
                            }
                        ]}
                    >
                    </ProFormText>
                    <ProFormText.Password
                        name="password"
                        placeholder={"请输入密码"}
                        label={"密码"}
                        rules={[
                            {
                                required:true,
                                message:"请输入密码"
                            }
                        ]}
                    />
                </>
            </LoginFormPage>
        </div>
    )
};


export default Login;