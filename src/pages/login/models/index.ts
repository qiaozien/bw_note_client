import {
    _get_artical,
} from "@/api";
import { Effect, Reducer } from 'umi';
export interface loginState {
    name: number;
    list: any[]
}
interface loginModelInterface {
    namespace: string;
    state: loginState;
    reducers: {
        changeName: Reducer<loginState>;
        changeList: Reducer<loginState>;
    };
    effects: {
        getArtical: Effect 
    }
}
const loginModel:loginModelInterface = {
    //  声明命名空间
    namespace:"loginModel",
    state:{
        name:0,
        list:[]
    },
    reducers:{
        changeName(state) {
            return {
                ...state,
                name:state!.name+1
            } as loginState
        },
        changeList(state,{payload}) {
            return {
                ...state,
                list:[...payload]
            } as loginState
        }
    },
    effects : {
        *getArtical (__,{ put , call}) {
            const data:{data:any[]} = yield call(_get_artical);
            yield put({
                type:"changeList",
                payload:[...data.data[0]]
            })
        }
    }
};


export default loginModel;