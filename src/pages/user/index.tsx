import { FC , useRef} from "react";
import { useRequest } from "ahooks";
import { _getUserList } from "@/api/users";
import { ProTable , ProColumns , ActionType } from "@ant-design/pro-components";
import BaseSelect  from "@/components/baseSelect";
import { Button } from "antd";
import SearchOutlined  from "@ant-design/icons";


const User:FC = () => {
    const actionRef = useRef<ActionType | undefined>();
    const { runAsync } = useRequest(_getUserList ,{
        manual:true
    });
    const columns:ProColumns = [
        {
            title: '账号',
            dataIndex: 'name'
        },
        {
            title:"邮箱",
            dataIndex:"email",
        },
        {
            title:"角色",
            dataIndex:"role",
            renderFormItem: (item , { type , defaultRender , ...rest}) => {
                // return 封装一个组件
                return <BaseSelect
                    title="角色"
                    {...rest}
                    options = {
                        [
                           {
                            title:"访客",
                            key:"visitor"
                           },
                           {
                            title:"管理员",
                            key:"admin"
                           }
                        ]
                    }
                />
            }
        },
        {
            title:"状态",
            dataIndex:"status",
            renderFormItem: (item , { type , defaultRender , ...rest}) => {
                return <BaseSelect
                    {...rest}
                    title="状态"
                    options={[
                        {
                            title:"已锁定",
                            key:"locked"
                        },
                        {
                            title:"可用",
                            key:"active"
                        }
                    ]}
                />
            }
        },
        {
            title:"注册日期",
            dataIndex:"createAt"
        },
        {
            title:"操作",
            render : () => {
                return <div>
                    <Button>禁用</Button>
                </div>
            }
        }
    ];
    return (
        <ProTable
            rowSelection={
                {
                    type:"checkbox"
                }
                
            }
            actionRef={actionRef}
            columns={columns} // 列的配置
            rowKey={"id"}
            pagination={{
                current:1,
                pageSize:10
            }}
            search={{
                searchText:"查询",
                span:5,
                optionRender: (searchConfig,formProps,dom) => {
                    return dom;
                }
            }}
            toolbar={{
                actions:[
                    <Button key="reload" icon={<SearchOutlined />}></Button>
                ]
            }}
            request= {
                async (options) => { // 初始化的创建表格执行， 分页 改变执行， 点击查询的时候 也会执行
                    console.log('--------request-----',options)
                    options = {
                        ...options,
                        page:options.current ,
                    };
                    delete options.current;
                    const { data } = await runAsync(options) ; // 手动发起请求
                    return {
                        data:data[0],
                        success:true,
                        total:data[1]
                    }
                }
            }
        ></ProTable>
    )
};


export default User;