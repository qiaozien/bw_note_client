import { Select } from "antd";

interface OptionItemType{
    title:string,
    key:string
};

interface Props {
    options:OptionItemType[],
    title?:string,
    onChange?: () => {}
}



const BaseSelect = ({options , title ="", onChange} : Props) => {
    return (
        <Select placeholder={"请选择"+ title}>
            {
                options.map((item:OptionItemType) => {
                    return <Select.Option key={item.key} onChange={onChange} value={item.key}>
                        {item.title}
                    </Select.Option>
                })
            }
        </Select>
    )
};



export default BaseSelect;