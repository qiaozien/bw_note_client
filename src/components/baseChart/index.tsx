import { EChartsType , init , EChartsOption , dispose } from "echarts";
import { useRef , useEffect } from "react";

type Types = "pie" | "bar" | "line";

interface OptionsType{
    type:Types,
    series:any[],
    xData?:any[],
    yData?:any[],
    chartData?:any
}

interface MyProps extends OptionsType{
    width?:number,
    height?:number,
    loading?:boolean
};
//  格式化数据
const formatOption = ({ 
    type ,
    series,
    xData,
    yData,
    chartData
}:OptionsType):EChartsOption => {
    switch (type){
        case "pie":
            return {
                ...chartData,
                series
            };
        case "bar":
        case "line":
        return {
            ...chartData,
            xAxis:{
                type:"category",
                data:xData
            },
            yAxis:{
                type:"value"
            },
            series
        }
    }
}

const BaseChart = ({
    type = "bar" ,
    series=[],// 图表主数据
    width=500,
    height=400,
    xData=[], // x轴数据
    yData=[],// y轴数据
    chartData={},
    loading=true

}:MyProps) => {
    const chartInstance = useRef<null | EChartsType>(null);
    const chartDom = useRef<null | HTMLDivElement>(null);
    useEffect(() => {
        if(chartDom.current){
            chartInstance.current = init(chartDom.current,"",{
                width,
                height
            });
        }
        return () => {
            chartInstance.current?.dispose(); // 图表和图表内组建的卸载
            chartInstance.current && dispose(chartInstance.current); // 卸载dom所有的方法
        }
    },[]);
    useEffect(() => {
        if(chartInstance.current){
            chartInstance.current.setOption(formatOption({
                type,
                series,
                xData,
                yData,
                chartData
            }));
        }
    },[type , series , xData , yData , chartInstance]);
    useEffect(() => {
        if(chartInstance.current){
            loading ? chartInstance.current.showLoading() : chartInstance.current.hideLoading()
        }
    },[loading , chartInstance]);
    return (
        <div ref={chartDom}>
           
        </div>
    )
};


export default BaseChart;


