// @ts-nocheck
import React from 'react';
import { ApplyPluginsType } from '/Users/qiaozi/Desktop/北京八维/实训/2001B/2001b/bw_note_admin/node_modules/umi/node_modules/@umijs/runtime';
import * as umiExports from './umiExports';
import { plugin } from './plugin';

export function getRoutes() {
  const routes = [
  {
    "path": "/",
    "component": require('/Users/qiaozi/Desktop/北京八维/实训/2001B/2001b/bw_note_admin/src/.umi/plugin-layout/Layout.tsx').default,
    "routes": [
      {
        "path": "/login",
        "component": require('@/pages/login/index').default,
        "menuRender": false,
        "headerRender": false,
        "exact": true
      },
      {
        "path": "/registry",
        "menuRender": false,
        "target": "_blank",
        "headerRender": false,
        "component": require('@/pages/registry/index').default,
        "exact": true
      },
      {
        "path": "/index",
        "component": require('@/pages/index/index').default,
        "name": "工作台",
        "exact": true
      },
      {
        "path": "/user",
        "component": require('@/pages/user/index').default,
        "name": "用户管理",
        "access": "isAdmin",
        "exact": true
      },
      {
        "path": "/",
        "redirect": "/login",
        "exact": true
      }
    ]
  }
];

  // allow user to extend routes
  plugin.applyPlugins({
    key: 'patchRoutes',
    type: ApplyPluginsType.event,
    args: { routes },
  });

  return routes;
}
